﻿------------------The CCK Validation Field Module----------------------------

CCK Validation adds a content field, that lets you add custom validation to
your content types. With the included Widget 'Textarea PHP Code' you can add
the first custom validation rules.

-------------------------Usage--------------------------------------------------

---------- General -----------------------------------

Before you can use CCK Validation Field, you'll need to get CCK and enable (at the
very least) the 'content' module. 

To add a validation field to a content type, go to administer > content >
content types, select the content type you want to add to, and click on the
'add field' tab. One of the field types available should be 'Validation', and it
should have one bullet point under it, labelled 'Textarea PHP Code'. If you select
this, give your field a name, and submit the form, you will get to the
configuration page for your new computed field.


--------Configuration ---------------------------------------

FIELD: CCK Validation
* Validation process -- Select this option to process validation on the node field's
  widget values or/and the node field's field values. When both are selected validation
  is run twice.
  <This setting can be overwritten by widget's $form["force_validation_process"]!>
  
  Example:
  ========
  Field value of date selection field
  	[field_date] => Array ( [0] => Array ( [value] => 2005-01-01T02:04:00 )
  Widget value of date selection field
		[field_date] => Array ( [0] => Array ( [value] => Array ( [mday] => 1 [mon] => 1 [year] => 2005 [hours] => 2 [minutes] => 4 ) )

* Check on -- Select on which node-operations validation shall be processed
	form_add: Add node - form view
	form_edit: Edit node - form view
	validate_add: Add node - submit validation
	validate_edit: Edit node - submit validation
  <This setting can be overwritten by widget's $form["force_check_on"]!>
		
* Save value -- Select to save a value for this field in the database.
	Default value is 1. Another value can be set in Widget via $form['value_to_save']
  <This setting can be overwritten by widget's $form["force_save_value"]!>

WIDGET: Textarea PHP Code
-------------------------

With the included widget 'Textarea PHP Code' you are able to create a custom
validation code. You will execute it via form_set_error() call.

* Validation Code -- This is the code that process PHP code validate the node's
  values and when needed, execute form_set_error() for Validation error messages.
  Available variables: $op, $node, $field, $node_field and $is_widget
  
------------- Adding Widgets ----------------------------------
  
Module developers can simply add new widgets to CCK Validation by using some predefined
widget setting fields.

Widget setting form fields for field-modification:
* validation_function (required!) [str]
* value_to_save [int]
* force_validation_process [array]
* force_check_on [array]
* force_save_value [bool]

Therefore you must ... :
	* hook_widget_info():
		* set 'field types' => array('cck_validation')
	* hook_widget_settings():
		* at least add a string-value or string-selection of a validation function to
		  hook_widget_settings' form $form['validation_function']:
	
		For Example: 
		$form['validation_function'] = array(
		  	'#type' => 'value',
		  	'#value' => '_cck_validation_run_code',
		);
		
		The function called has to look like
			function function_name($op, $node, $field, $node_field, $is_widget) {...
		
		and has to return form_set_error for creating validaton error messages.
	* hook_widget():
		simply add 'return cck_validation_widget($op,$node,$field,$node_field);' within your
		hook_widget for implementing the cck validation features
		 
--------Code Examples for Textarea PHP Code------------------------------------------

Test minimum length of a textfield (e.g. 4 chars)
-------------------------------------------------

- Validiation Code:
if (strlen($node->field_text[0]['value'])<4){
  form_set_error('field_text','text has to have a minimum of 4 chars!');
}

- Process validation
  can be set to both (field or widget)
  
- Check On
  "Add node - submit validation" & "Edit node - submit validation"
  
Test date's year
----------------

- Validiation Code:
if ($node->field_date[0]['value']['year'] >= 2000){
  form_set_error('field_date','Date has to be before year 2000');
}

- Process validation
  WIDGET - 	for this code it has to be set to widget, because in field-validation
  				 	date-array isn't present anymore.

- Check On
  "Add node - submit validation" & "Edit node - submit validation"

